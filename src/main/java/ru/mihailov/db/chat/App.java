package ru.mihailov.db.chat;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ru.mihailov.db.chat.model.ChatGroup;
import ru.mihailov.db.chat.model.ChatMessage;
import ru.mihailov.db.chat.model.ChatUser;
import ru.mihailov.db.chat.service.ChatGroupService;
import ru.mihailov.db.chat.service.ChatMessageService;
import ru.mihailov.db.chat.service.ChatUserService;
import ru.mihailov.db.chat.types.UserRole;

public class App {

    public static void main(String[] args) throws Exception {
        ChatGroupService chatGroupService = new ChatGroupService();
        ChatGroup chatGroup = new ChatGroup();
        chatGroup.setName("Common group");
        Set<ChatGroup> groups = new HashSet<>();
        groups.add(chatGroup);

        ChatUserService chatUserService = new ChatUserService();
        ChatUser chatUser1 = new ChatUser();
        chatUser1.setLogin("user1");
        chatUser1.setName("Trump");
        chatUser1.setPassword("President");
        chatUser1.setRole(UserRole.ADMIN);
        chatUser1.setGroups(groups);

        chatUser1 = chatUserService.saveChatUser(chatUser1);
        System.out.println("New ChatUser = " + chatUser1);
        chatUser1 = chatUserService.getChatUserById(1);
        System.out.println("getChatUserById = " + chatUser1);

        chatUser1 = chatUserService.getChatUserByName("Trump");
        System.out.println("getChatUserByName = " + chatUser1);

        chatUser1 = chatUserService.getChatUserByLogin("user1");
        System.out.println("getChatUserByLogin = " + chatUser1);

        chatUser1 = chatUserService.getChatUserById(1);
        System.out.println("getChatUserById = " + chatUser1);

        ChatMessageService chatMessageService = new ChatMessageService();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessageDate(new Date());
        chatMessage.setChatGroup(chatGroup);
        chatMessage.setChatUser(chatUser1);
        chatMessage.setMessageText("test message of user1");
        chatMessageService.saveChatMessage(chatMessage);
        List<ChatMessage> messages = chatMessageService.getChatMessagesByGroupId(chatGroup.getId());
        messages.forEach(message -> System.out.println("message = " + message.getMessageText()));
    }

}
