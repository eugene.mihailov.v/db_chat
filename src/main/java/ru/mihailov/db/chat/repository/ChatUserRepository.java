package ru.mihailov.db.chat.repository;

import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.query.Query;
import ru.mihailov.db.chat.model.ChatUser;

public class ChatUserRepository {

    public ChatUser getChatUserById(Session session, Integer id) {
        return session.find(ChatUser.class, id);
    }

    public ChatUser saveChatUser(Session session, ChatUser chatUser) {
        if (chatUser.getId() == null) {
            session.persist(chatUser);
        } else {
            chatUser = (ChatUser) session.merge(chatUser);
        }
        return chatUser;
    }

    public void deleteChatUser(Session session, ChatUser chatUser) {
        if (session.contains(chatUser)) {
            session.remove(chatUser);
        } else {
            session.merge(chatUser);
        }
    }

    public ChatUser getChatUserByName(Session session, String name) {
        TypedQuery<ChatUser> q =
                session.createQuery("SELECT b FROM ChatUser b WHERE b.name = :name", ChatUser.class);
        q.setParameter("name", name);
        try {
            return q.getSingleResult();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public ChatUser getChatUserByLogin(Session session, String login) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ChatUser> chatUserCriteriaQuery = cb.createQuery(ChatUser.class);
        Root<ChatUser> root = chatUserCriteriaQuery.from(ChatUser.class);
        chatUserCriteriaQuery.select(root).where(cb.equal(root.get("login"), login));

        try {
            Query<ChatUser> query = session.createQuery(chatUserCriteriaQuery);
            List<ChatUser> results = query.getResultList();
            return results != null && results.size() > 0 ? results.get(0) : null;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

}
