package ru.mihailov.db.chat.repository;

import javax.persistence.TypedQuery;
import org.hibernate.Session;
import ru.mihailov.db.chat.model.ChatGroup;

public class ChatGroupRepository {

    public ChatGroup getChatGroupById(Session session, Integer id) {
        return session.get(ChatGroup.class, id);
    }

    public ChatGroup saveChatGroup(Session session, ChatGroup ChatGroup) {
        if (ChatGroup.getId() == null) {
            session.persist(ChatGroup);
        } else {
            ChatGroup = (ChatGroup) session.merge(ChatGroup);
        }
        return ChatGroup;
    }

    public void deleteChatGroup(Session session, ChatGroup ChatGroup) {
        if (session.contains(ChatGroup)) {
            session.remove(ChatGroup);
        } else {
            session.merge(ChatGroup);
        }
    }

    public ChatGroup getChatGroupByName(Session session, String name) {
        TypedQuery<ChatGroup> q =
                session.createQuery("SELECT b FROM ChatGroup b WHERE b.name = :name", ChatGroup.class);
        q.setParameter("name", name);
        try {
            return q.getSingleResult();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

}
