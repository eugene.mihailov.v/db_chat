package ru.mihailov.db.chat.repository;

import java.util.List;
import javax.persistence.TypedQuery;
import org.hibernate.Session;
import ru.mihailov.db.chat.model.ChatMessage;
import ru.mihailov.db.chat.model.ChatUser;

public class ChatMessageRepository {

    public ChatMessage getChatMessageById(Session session, Integer id) {
        return session.find(ChatMessage.class, id);
    }

    public ChatMessage saveChatMessage(Session session, ChatMessage ChatMessage) {
        if (ChatMessage.getId() == null) {
            session.persist(ChatMessage);
        } else {
            ChatMessage = (ChatMessage) session.merge(ChatMessage);
        }
        return ChatMessage;
    }

    public void deleteChatMessage(Session session, ChatMessage ChatMessage) {
        if (session.contains(ChatMessage)) {
            session.remove(ChatMessage);
        } else {
            session.merge(ChatMessage);
        }
    }

    public List<ChatMessage> getChatMessagesByGroupId(Session session, Integer groupId) {
        TypedQuery<ChatMessage> q =
                session.createQuery("SELECT b FROM ChatMessage b WHERE b.chatGroup.id = :chatGroup", ChatMessage.class);
        q.setParameter("chatGroup", groupId);
        try {
            return q.getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

}
