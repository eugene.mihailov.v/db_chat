package ru.mihailov.db.chat.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

import static ru.mihailov.db.chat.config.DatabaseConfig.DB_TABLE_PREFIX;

@Data
@Entity
@Table(name = DB_TABLE_PREFIX + "groups")
public class ChatGroup implements Serializable {

    private static final long serialVersionUID = -1798070786993154676L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "chatGroup", cascade= CascadeType.ALL)
    private Set<ChatMessage> messages;

    @ManyToMany(mappedBy = "groups")
    private Set<ChatUser> users;

}
