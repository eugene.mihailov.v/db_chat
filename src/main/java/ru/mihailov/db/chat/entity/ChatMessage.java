package ru.mihailov.db.chat.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

import static ru.mihailov.db.chat.config.DatabaseConfig.DB_TABLE_PREFIX;

@Data
@Entity
@Table(name = DB_TABLE_PREFIX + "messages")
public class ChatMessage implements Serializable {

    private static final long serialVersionUID = -1798070786993154677L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "message_text")
    private String messageText;

    @Column(name = "message_date")
    private Date messageDate;

    @ManyToOne
    @JoinColumn(name="chatGroup_id", nullable=false)
    private ChatGroup chatGroup;

    @ManyToOne
    @JoinColumn(name="chatUser_id", nullable=false)
    private ChatUser chatUser;

}
