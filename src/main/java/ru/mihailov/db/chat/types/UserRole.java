package ru.mihailov.db.chat.types;

public enum UserRole {

    /**
     * Administrator.
     */
    ADMIN,

    /**
     * Just user.
     */
    USER

}
