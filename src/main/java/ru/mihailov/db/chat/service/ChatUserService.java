package ru.mihailov.db.chat.service;

import org.hibernate.Session;
import ru.mihailov.db.chat.model.ChatUser;
import ru.mihailov.db.chat.repository.ChatUserRepository;

public class ChatUserService extends AbstractService {

    private ChatUserRepository chatUserRepository = new ChatUserRepository();;

    public ChatUser getChatUserById(Integer id) {
        if (id == null) {
            System.out.println("ChatUser.findById: id must not be null!");
            return null;
        }
        ChatUser chatUser = null;
        try {
            chatUser = chatUserRepository.getChatUserById(openSession(), id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeSession();
        }
        return chatUser;
    }

    public ChatUser saveChatUser(ChatUser chatUser) {
        if (chatUser == null) {
            System.out.println("ChatUser.save: the parameter chatUser must not be null!");
            return null;
        }
        Session session = openSession();
        session.getTransaction().begin();
        try {
            chatUser = chatUserRepository.saveChatUser(session, chatUser);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println("Error to save chatUser: " + e.getMessage());
        } finally {
            closeSession();
        }
        return chatUser;
    }

    public void deleteChatUser(ChatUser chatUser) {
        if (chatUser == null) {
            System.out.println("ChatUser.delete: the parameter chatUser must not be null!");
        }
        Session session = openSession();
        session.getTransaction().begin();
        try {
            chatUserRepository.deleteChatUser(session, chatUser);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println("Error to delete chatUser: " + e.getMessage());
        } finally {
            closeSession();
        }
    }

    public ChatUser getChatUserByName(String name) {
        ChatUser chatUser = null;
        try {
            chatUser = chatUserRepository.getChatUserByName(openSession(), name);
        } finally {
            closeSession();
        }
        return chatUser;
    }

    public ChatUser getChatUserByLogin(String login) {
        ChatUser chatUser = null;
        try {
            chatUser = chatUserRepository.getChatUserByLogin(openSession(), login);
        } finally {
            closeSession();
        }
        return chatUser;
    }

}
