package ru.mihailov.db.chat.service;

import org.hibernate.Session;
import ru.mihailov.db.chat.session.HibernateUtil;

public class AbstractService {

    private Session session;

    protected Session openSession() {
        session = HibernateUtil.getSessionFactory().openSession();
       return session;
    }

    protected void closeSession() {
        if (session != null) {
            session.close();
        }
    }

}
