package ru.mihailov.db.chat.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import ru.mihailov.db.chat.model.ChatMessage;
import ru.mihailov.db.chat.repository.ChatMessageRepository;

public class ChatMessageService extends AbstractService {

    private ChatMessageRepository chatMessageRepository = new ChatMessageRepository();;

    public ChatMessage getChatMessageById(Integer id) {
        if (id == null) {
            System.out.println("ChatMessage.findById: id must not be null!");
            return null;
        }
        ChatMessage chatMessage = null;
        try {
            chatMessage = chatMessageRepository.getChatMessageById(openSession(), id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeSession();
        }
        return chatMessage;
    }

    public ChatMessage saveChatMessage(ChatMessage chatMessage) {
        if (chatMessage == null) {
            System.out.println("ChatMessage.save: the parameter ChatMessage must not be null!");
            return null;
        }
        Session session = openSession();
        session.getTransaction().begin();
        try {
            if (chatMessage.getMessageDate() == null) {
                chatMessage.setMessageDate(new Date());
            }
            chatMessage = chatMessageRepository.saveChatMessage(session, chatMessage);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println("Error to save ChatMessage: " + e.getMessage());
        } finally {
            closeSession();
        }
        return chatMessage;
    }

    public void deleteChatMessage(ChatMessage chatMessage) {
        if (chatMessage == null) {
            System.out.println("ChatMessage.delete: the parameter ChatMessage must not be null!");
        }
        Session session = openSession();
        session.getTransaction().begin();
        try {
            chatMessageRepository.deleteChatMessage(session, chatMessage);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println("Error to delete ChatMessage: " + e.getMessage());
        } finally {
            closeSession();
        }
    }

    public List<ChatMessage> getChatMessagesByGroupId(Integer groupId) {
        List<ChatMessage> messages = new ArrayList<>();
        try {
            messages = chatMessageRepository.getChatMessagesByGroupId(openSession(), groupId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeSession();
        }
        return messages;
    }
}
