package ru.mihailov.db.chat.service;

import org.hibernate.Session;
import ru.mihailov.db.chat.model.ChatGroup;
import ru.mihailov.db.chat.repository.ChatGroupRepository;

public class ChatGroupService extends AbstractService {

    private ChatGroupRepository chatGroupRepository = new ChatGroupRepository();;

    public ChatGroup getChatGroupById(Integer id) {
        if (id == null) {
            System.out.println("ChatGroup.findById: id must not be null!");
            return null;
        }
        ChatGroup chatGroup = null;
        try {
            chatGroup = chatGroupRepository.getChatGroupById(openSession(), id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeSession();
        }
        return chatGroup;
    }

    public ChatGroup saveChatGroup(ChatGroup chatGroup) {
        if (chatGroup == null) {
            System.out.println("ChatGroup.save: the parameter ChatGroup must not be null!");
            return null;
        }
        Session session = openSession();
        session.getTransaction().begin();
        try {
            chatGroup = chatGroupRepository.saveChatGroup(session, chatGroup);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println("Error to save ChatGroup: " + e.getMessage());
        } finally {
            closeSession();
        }
        return chatGroup;
    }

    public void deleteChatGroup(ChatGroup chatGroup) {
        if (chatGroup == null) {
            System.out.println("ChatGroup.delete: the parameter ChatGroup must not be null!");
        }
        Session session = openSession();
        session.getTransaction().begin();
        try {
            chatGroupRepository.deleteChatGroup(session, chatGroup);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println("Error to delete ChatGroup: " + e.getMessage());
        } finally {
            closeSession();
        }
    }

    public ChatGroup getChatGroupByName(String name) {
        ChatGroup chatGroup = null;
        try {
            chatGroup = chatGroupRepository.getChatGroupByName(openSession(), name);
        } finally {
            closeSession();
        }
        return chatGroup;
    }

}
