package ru.mihailov.db.chat.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import ru.mihailov.db.chat.types.UserRole;

import static ru.mihailov.db.chat.config.DatabaseConfig.DB_TABLE_PREFIX;

@Data
@Entity
@Table(name = DB_TABLE_PREFIX + "users")
public class ChatUser implements Serializable {

    private static final long serialVersionUID = -1798070786993154678L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    private String login;

    private String password;

    private UserRole role;

    @OneToMany(mappedBy = "chatUser")
    private Set<ChatMessage> messages;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(
            name = DB_TABLE_PREFIX + "users_groups",
            joinColumns = { @JoinColumn(name = "chatUser_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "chatGroup_id", nullable = false, updatable = false) }
    )
    private Set<ChatGroup> groups;

    @Override
    public String toString() {
        return "ChatUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }

}
